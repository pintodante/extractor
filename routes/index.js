var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var parseString = require('xml2js').parseString;
var QPromise = require('promise');
var request = require("request");
const servers = require('../config/geoservers_1');
var DOWNLOAD_DIR = './downloads/';
const errorLog = require('../utils/logger').errorlog;
const successlog = require('../utils/logger').successlog;

router.get('/', function (req, res, next) {
    //var file_url = 'url+ wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typeName=+filename+&outputFormat=shape-zip';
    var file_url = 'https://geo.gob.bo/geoserver/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typeName=aasana:aerodromos&outputFormat=shape-zip';
    var file_url_wfs = 'https://geo.gob.bo/geoserver/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities';
    var file_url_wfs_2 = 'http://p.sicirec.enbolivia.com:8080/geoserver/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities';
    var get_wfs_layers = 'wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities';
    var promises = [];
//    console.log(servers.servers);
    servers.servers.forEach(function (val, key) {
        promises = [];
        console.log(key);
        promises.push(descarga(val));
        console.log(promises);
//        QPromise.all(promises).then(function (str) {
//            console.log('Saved everything1');
//            //         console.log(str);
//        }, function (error) {
//            console.log('ERROR', error);
//        }).then(function () {
//
//            console.log('Saved everything2');
//        }).done(function () {
//            console.log('Saved everything3');
//
//        });
//        descarga(val);
    });
// request para descargar el shape zip apartir de obtener los datos del geoserver
// request({url: file_url, encoding: null}, function(err, resp, body) {
//     if(!err){
//         var file = fs.createWriteStream(DOWNLOAD_DIR + 'aerodromos.zip');
//         var buff = new Buffer(body);
//          file.write(buff,function(err){
//              console.log(err);
//          });
//           file.end();
//            console.log(' downloaded to ' + DOWNLOAD_DIR);
//     }else{
//         console.log("No results error.",err);
//     }
// });


//    download_file_wget(file_url);
//var download = function(url, dest, cb) {
//  var file = fs.createWriteStream(dest);
//  var request = https.get(url, function(response) {
//    response.pipe(file);
//    file.on('finish', function() {
//      file.close(cb);  // close() is async, call cb after close completes.
//    });
//  }).on('error', function(err) { // Handle errors
//    fs.unlink(dest); // Delete the file async. (But we don't check the result)
//    if (cb) cb(err.message);
//  });
//};

    function descarga(options) {
        return new QPromise((resolve, reject) => {
        console.log(options.url + get_wfs_layers);
            request({url: options.url + get_wfs_layers, headers: {'Content-Type': 'text/xml'},rejectUnauthorized: false}, function (err, data, body) {
                if (!err) {
                    resolve(body);
                } else {
                    reject(new Error('geoserver ' + options.url + 'ERROR : ' + err));
                }

            });
        }).then(function (resolve) {
//            console.log('entreo');
            var promises = [];
//            return new QPromise((res, rej) => {
            promises.push(new QPromise((res, rej) => {
                parseString(resolve, function (err, result) {
                    fs.exists(DOWNLOAD_DIR + options.shortName, (exists) => {   
                        var filename = DOWNLOAD_DIR + options.shortName;
                        if (!exists) {
                            fs.mkdir(filename, function () {
                                successlog.info(`se creo la carpeta del nodo: ${filename}`);
//                                console.log('se creo la carpeta del nodo', filename);
                            });
                        }
                        var json = JSON.parse(JSON.stringify(result));
                        var layers = json['wfs:WFS_Capabilities']['FeatureTypeList'][0]['FeatureType'];
                        successlog.info(`descarga del servidor: ${options.name}`);
//                        console.log('descarga del servidor:', options.name);
                        console.log(layers.length);
                        layers.forEach(function (val, key) {
                            //** apartir de aqui hay que iterar o crear promises
                            //var url_shape=options.url+ 'wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typeName='+json['wfs:WFS_Capabilities']['FeatureTypeList'][0]['FeatureType'][0]['Name']+'&outputFormat=shape-zip';
                            //var shape_name=json['wfs:WFS_Capabilities']['FeatureTypeList'][0]['FeatureType'][0]['Title'];
                            var url_shape = options.url + 'wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typeName=' + val['Name'] + '&srsName=EPSG:4326&outputFormat=shape-zip';
                            var shape_name = val['Title'];
//                            shape_name = shape_name.toString().replace(/[^\x00-\x7F]/g, "_");
                            shape_name = getCleanedString(shape_name.toString());
                            
                            if(!fs.existsSync(DOWNLOAD_DIR + options.shortName + '/' + shape_name + '.zip')){
                                request({url: url_shape, encoding: null,rejectUnauthorized: false}, function (err, resp, body) {
                                    if (!err) {
                                        var zip_name = DOWNLOAD_DIR + options.shortName + '/' + shape_name + '.zip';
                                        var file = fs.createWriteStream(zip_name);
                                        var buff = new Buffer(body);
                                        file.write(buff, function (err, resp) {
                                            if (!err) {
                                            } else {
                                                errorLog.error(`Error al guardar el shapefile : ${err}`);
    //                                            console.log('Error al guardar el shapefile', err);
                                            }
                                        });
                                        file.end(function (res) {
                                            var size = getFilesizeInBytes(zip_name);
                                            successlog.info(`se descargo el layer : ${shape_name}, ${size} ,${options.url}`);
    //                                        console.log('se descargo el layer ', shape_name, size);
                                        });
                                    } else {
                                        errorLog.error(`Error al descargar el shapefile : ${err}, ${shape_name}, ${options.url}, ${url_shape} `);
    //                                    console.log("No results error.", err,shape_name,options.url,url_shape);
                                    }
                                });
                            }
                            //**
                        });

                    });

                });
                res('Termino la descarga de las capas');
            }).then(function (res) {
                successlog.info(res);
//                console.log(res);
            }, function (err) {
                if (!err) {
                    console.log('ERROR', err);
                }

//            });
            }));
            return promises;
        }, function (err) {
                console.log('ERROR', err);
            if (!err) {
                errorLog.error(`ERROR : ${err}`);
//                console.log('ERROR', err);
            }
        });
    }
    function getFilesizeInBytes(filename) {
        const stats = fs.statSync(filename);
        const size = stats.size;
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'KB', 'MB', 'GB', 'TB'][i];
//        return fileSizeInBytes;
    }
    function getCleanedString(cadena) {
        // Definimos los caracteres que queremos eliminar
        var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

        // Los eliminamos todos
        for (var i = 0; i < specialChars.length; i++) {
            cadena = cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
        }

        // Lo queremos devolver limpio en minusculas
        cadena = cadena.toLowerCase();

        // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
        cadena = cadena.replace(/ /g, "_");

        // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
        cadena = cadena.replace(/á/gi, "a");
        cadena = cadena.replace(/é/gi, "e");
        cadena = cadena.replace(/í/gi, "i");
        cadena = cadena.replace(/ó/gi, "o");
        cadena = cadena.replace(/ú/gi, "u");
        cadena = cadena.replace(/ñ/gi, "n");
        return cadena;
    }
//descarga(file_url_wfs_2);
    res.render('index', {title: 'descargado'});
});

module.exports = router;

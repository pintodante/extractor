FROM node:carbon
WORKDIR /src
COPY package.json .
RUN npm install && touch download.log
COPY . .
EXPOSE 3000
CMD echo "starting in $(date)" && npm start